#===================================================================================================
# Walter Hannah - Lawrence Livermore National Lab
#===================================================================================================
import os
import subprocess as sp
import xarray as xr
import numpy as np
import numba
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def apply_map(data_native, row, col, wgt):
   data_remap = numpy.zeros(max(row).values)
   for k in range(len(wgt)):
      data_remap[row[k]-1] = d_remap[row[k]-1] + data_native[col[k]-1] * wgt[k]
   return data_remap
#---------------------------------------------------------------------------------------------------
def remap(vname, mapfile, inputfile, outputfile):
   """ """
   # Read inputs
   ds_map = xarray.open_dataset(mapfile)
   ds_in  = xarray.open_dataset(inputfile).isel(time=0)
   wgt = ds_map['S']
   row = ds_map['row']
   col = ds_map['col']
   data_native = ds_in[vname]
   # Read coordinate variables
   lat = ds_map['yc_b']
   lon = ds_map['xc_b']
   # Apply map
   data_remap = xarray.DataArray( apply_map(data_native, row.values, col.values, wgt.values), dims=lat.dims)


#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------