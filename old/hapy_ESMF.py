#===================================================================================================
# Walter Hannah - Lawrence Livermore National Lab
# 
#  create_grid_rectilinear
#  regrid_field
#===================================================================================================
import ESMF
import numpy as np
#---------------------------------------------------------------------------------------------------
#---------------------------------------------------------------------------------------------------
def create_grid_rectilinear(lat1,lat2,lon1,lon2,dx) :
   doarea = True

   min_x = float(lon1)
   max_x = float(lon2)
   min_y = float(lat1)
   max_y = float(lat2)

   x_span = max_x - min_x
   y_span = max_y - min_y

   lb_x = 0
   lb_y = 0
   ub_x = int( x_span/dx )
   ub_y = int( y_span/dx )

   cellwidth_x = (max_x-min_x)/(ub_x-lb_x)
   cellwidth_y = (max_y-min_y)/(ub_y-lb_y)

   cellcenter_x = cellwidth_x/2
   cellcenter_y = cellwidth_y/2

   max_index = np.array([ub_x,ub_y])

   grid = ESMF.Grid(max_index, coord_sys=ESMF.CoordSys.CART)

   ##     CORNERS
   grid.add_coords(staggerloc=[ESMF.StaggerLoc.CORNER])

   # get the coordinate pointers and set the coordinates
   [x,y] = [0,1]
   gridCorner = grid.coords[ESMF.StaggerLoc.CORNER]

   for i in range(gridCorner[x].shape[x]):
     gridCorner[x][i, :] = float(i)*cellwidth_x + \
         min_x + grid.lower_bounds[ESMF.StaggerLoc.CORNER][x] * cellwidth_x
         # last line is the pet specific starting point for this stagger and dim

   for j in range(gridCorner[y].shape[y]):
      gridCorner[y][:, j] = float(j)*cellwidth_y + \
         min_y + grid.lower_bounds[ESMF.StaggerLoc.CORNER][y] * cellwidth_y
         # last line is the pet specific starting point for this stagger and dim

   ##     CENTERS
   grid.add_coords(staggerloc=[ESMF.StaggerLoc.CENTER])

   # get the coordinate pointers and set the coordinates
   [x,y] = [0,1]
   gridXCenter = grid.get_coords(x, staggerloc=ESMF.StaggerLoc.CENTER)
   gridYCenter = grid.get_coords(y, staggerloc=ESMF.StaggerLoc.CENTER)

   for i in range(gridXCenter.shape[x]):
      gridXCenter[i, :] = float(i)*cellwidth_x + cellwidth_x/2.0 + \
         min_x + grid.lower_bounds[ESMF.StaggerLoc.CENTER][x] * cellwidth_x
         # last line is the pet specific starting point for this stagger and dim

   for j in range(gridYCenter.shape[y]):
      gridYCenter[:, j] = float(j)*cellwidth_y + cellwidth_y/2.0 + \
         min_y + grid.lower_bounds[ESMF.StaggerLoc.CENTER][y] * cellwidth_y
         # last line is the pet specific starting point for this stagger and dim


   # if doarea:
   #    grid.add_item(ESMF.GridItem.AREA)
   #    area = grid.get_item(ESMF.GridItem.AREA)
   #    area[:] = 5.0

   return grid

#-------------------------------------------------------------------------------
def regrid_field(srcfield,dst_grid) :

   # dstfield = ESMF.Field(dst_grid, 'dstmesh', meshloc=ESMF.MeshLoc.ELEMENT)
   dstfield = ESMF.Field(dst_grid, 'dstfield', staggerloc=ESMF.StaggerLoc.CENTER)

   regrid = ESMF.Regrid(srcfield, dstfield, \
                        # regrid_method=ESMF.RegridMethod.CONSERVE, \
                        regrid_method=ESMF.RegridMethod.BILINEAR, \
                        unmapped_action=ESMF.UnmappedAction.IGNORE)

   dstfield = regrid(srcfield, dstfield)

   return dstfield
#-------------------------------------------------------------------------------